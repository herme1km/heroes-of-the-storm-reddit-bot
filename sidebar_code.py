from pprint import pprint
import requests
import json
from lxml import html
from lxml.cssselect import CSSSelector
import requests
from datetime import datetime
import string
import praw
import time
#########################################################
####Get's the current sidebar text from the subreddit####
#########################################################
def getCurrentSubSideBar():
	
	sideBarData = requests.get("http://www.reddit.com/r/heroesofthestorm/about/.json")
	while(sideBarData.text == '{"error": 429}'):
		print "We entered the wait time man"
		time.sleep(900)
		sideBarData = requests.get("http://www.reddit.com/r/heroesofthestorm/about/.json")

	sideBarData = sideBarData.json()
	sideBarData = sideBarData['data']
	currentSideBar = sideBarData['description']
	return currentSideBar

##############################################################
####Gets the current free rotation from the Heroes Forums#####
####Replaces the text in the sidebar with the new rotation####
##############################################################
def getCurrentFreeRotation():
	page = requests.get('http://us.battle.net/heroes/en/forum/topic/17936383460')
	tree = html.fromstring(page.text)

	results = CSSSelector('div.post-detail li')
	preFreeRotation = results(tree)

	freeRotation = [Hero.text for Hero in preFreeRotation]

	for i in range(7):
		freeRotation[i] = freeRotation[i].split("(", 1)[0]
		freeRotation[i] = freeRotation[i].replace(" ", "")
		#saleRotation[i] = saleRotation[i].replace(u"\u2019", "'")

	return freeRotation

#####################################################################
####Starts the text replacement by building the new string to add####
#####################################################################
def buildNewFreeRotationSideBarText(freeRotationArgument):
	freeRotation = freeRotationArgument
	text_to_replace = []
	count = 0
	current_date = datetime.today()
	current_month = current_date.month
	current_day = current_date.day
	current_year = current_date.year

	months = ['Janurary', 'Feburary', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
	current_month = months[current_month-1]

	current_date = current_month + " " + str(current_day) + " " + str(current_year)

	text_to_replace.append("")
	text_to_replace.append("Week of Tuesday, %s" % (str(current_date)))
	text_to_replace.append("")
	for i in range(10):
		if i in range(0, 4) or i in range (5, 8):
			temp = freeRotation[count].lower()
			temp1 = freeRotation[count]
			text_to_replace.append("[](http://us.battle.net/heroes/en/heroes/%s/#free_rotation#%s)" % (temp, temp1))
			count = count + 1

		if i == 4 or i == 8:
			text_to_replace.append("[](#spacer)")

		if i == 9:
			text_to_replace.append("[](#bottom)")

	return text_to_replace

####################################################################################
####Replaces the text by finding the line #Free Hero Rotation and starting There####
####################################################################################
def assembleNewFreeRotationSideBarText(currentSideBarArgument, freeRotationArgument):

	currentSideBar = currentSideBarArgument
	text_to_replace = freeRotationArgument

	currentSideBar = string.split(currentSideBar, '\n')

	for i in range(len(currentSideBar)):
		if currentSideBar[i] == "#Free Hero Rotation":
			for j in range(13):
				currentSideBar[i+j+1] = text_to_replace[j]

	return currentSideBar
######################################################
####Gets the for sale items from the Heroes Forums####
######################################################
def getCurrentSaleRotation():
	page = requests.get('http://us.battle.net/heroes/en/forum/topic/18183929301')
	tree = html.fromstring(page.text)

	results = CSSSelector('div.post-detail li:contains("Sale")')
	preSaleItems = results(tree)

	saleRotation = [SaleItem.xpath("string()") for SaleItem in preSaleItems]

	for i in range(len(saleRotation)):
		saleRotation[i] = saleRotation[i].replace(u"\u2019", "'")

	return saleRotation

##################################################################################
###Startes the replacement text string builder. Checks if the month rolls over####
##################################################################################
def buildNewSaleRotationSideBarText(saleRotationArgument):

	saleRotation = saleRotationArgument

	text_to_replace = []
	count = 0
	current_date = datetime.today()
	current_month = current_date.month
	current_day = current_date.day
	current_year = current_date.year
	months = ['Janurary', 'Feburary', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
	monthsDict = {'Janurary':31, 'Feburary':28, 'March':31, 'April':30, 'May':31, 'June':30, 'July':31, 'August':31, 'September':30, 'October':31, 'November':30, 'December':31}

	######CHECK IF DAY'S ROLLED OVER###########
	current_day_second = current_day + 7
	next_month = months[current_month]
	current_month = months[current_month-1]
	if current_day_second > monthsDict[current_month]:
		current_day_second = current_day_second - monthsDict[current_month]

		saleDate = "#Weekly Sale: {0} {1} - {2} {3}, {4}".format((str(current_month)), (str(current_day)), (str(next_month)), (str(current_day_second)), (str(current_year)))

	else:
		saleDate = "#Weekly Sale: {0} {1} - {2} {3}, {4}".format((str(current_month)), (str(current_day)), (str(current_month)), (str(current_day_second)), (str(current_year)))

	text_to_replace = []
	text_to_replace.append(saleDate)
	count = 0
	for i in range(7):
		if i % 2 == 0:
			text_to_replace.append("")
		else:
			text_to_replace.append(saleRotation[count])
			count = count + 1

	return text_to_replace

####################################################################################
####Replaces the text by finding the line #Free Hero Rotation and starting There####
####################################################################################
def assembleNewSaleRotationSideBarText(currentSideBarArgument, saleRotationArgument):

	currentSideBar = currentSideBarArgument
	text_to_replace = saleRotationArgument

	for i in range(len(currentSideBar)):
		if currentSideBar[i] == "#Free Hero Rotation":
			for j in range(7):
				currentSideBar[i+j+16] = text_to_replace[j]


##############################################################
####Resets the sidebar as a string to be posted to the sub####
##############################################################
	sidebar = ""

	for i in range(len(currentSideBar)):
		sidebar = sidebar + "\n" + currentSideBar[i]

	

	return sidebar


#############################################################
def postToSubReddit(sidebar):
	#Initialize PRAW and login
	r = praw.Reddit(user_agent='Heroes FreeRotation Sidebar Update')
	r.login('hermes13','green12?')
	#Grab the current settings
	settings = r.get_subreddit('heroesofthestorm').get_settings()
	#Update the sidebar
	settings['description'] = sidebar
	settings = r.get_subreddit('heroesofthestorm').update_settings(description=settings['description'])
	print "posted"


######################################################################################################
def isTimeToStartUpdating():
	currentTime = datetime.now()
	weekday = currentTime.today().weekday()
	if weekday == 1:
		return True
######################################################################################################
# while(True):
# 	isItMonday = isTimeToStartUpdating()

# 	while(isItMonday == False):
# 		sleep(14400)
# 		isItMonday = isTimeToStartUpdating()

	
currentSideBar = getCurrentSubSideBar()
freeRotation = getCurrentFreeRotation()

newFreeRotationTextBlock = buildNewFreeRotationSideBarText(freeRotation)
newSideBar = assembleNewFreeRotationSideBarText(currentSideBar, newFreeRotationTextBlock)

saleRotation = getCurrentSaleRotation()
newSaleRotationTextBlock = buildNewSaleRotationSideBarText(saleRotation)
newSideBar = assembleNewSaleRotationSideBarText(newSideBar, newSaleRotationTextBlock)

print newSideBar
postToSubReddit(newSideBar)






